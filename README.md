# README #

nolabills is a utility scraping application, built on the web2py framework and designed to work with the use of several Python modules:

* Selenium

* BeautifulSoup

* pyVirtualDisplay

* pyAutoGUI

* various dependencies of the above packages

Because the application uses a headless browser, it solves the problem of downloading PDFs generated on-the-fly by utility company websites.

The project is at a standstill as I've determined that its intended audience is unlikely to use it anytime soon. Feel free to fork or use as you find it helpful.
# -*- coding: utf-8 -*-

import operator # needed for manager_status variable

response.title = 'nolabills'

def jspage():
    link = URL('static', 'main.js')
    return dict(link=link)

def index():
    """
    For registered users, redirects to the requests page
    For unregistered users, displays intro message
    """
    
    signup = URL(a='nolabills', c='default', f='user/register')
    
    return dict(is_logged_in=auth.is_logged_in(), signup=signup)

@auth.requires_membership('manager')
def manage():
    """
    This page, accessible by only managers, shows the current employees
    of a company as well as any users who are registering themselves as
    employees of the company.
    From this page a manager can approve a pending user's membership,
    as well as revoke the membership of a user on the current employee list.
    """
    pending_list = []
    approved_list = []
    if request.post_vars:
        for each in request.post_vars:
            key = request.post_vars.keys()[0]
            underscore = key.find('_')
            id = key[0:underscore]
            change = key[underscore+1:len(key)]
            change = None if change == "approve" else "unapproved"
            # modify db accordingly
            record = db(db.auth_user.id==id).select().first()
            record.registration_key=change
            record.update_record()

    for row in db(db.auth_user.company==auth.user.company).select():
        if row.registration_key: # if pending approval by manager
            pending_list.append({row.id:{"first":row.first_name,"last":row.last_name}})
        else:
            manager_status = db(db.auth_membership.user_id==row.id).select(db.auth_membership.group_id)
            manager_status = True if operator.contains(str(manager_status), '2') else False # requires that the manager group number is 2... could break
            approved_list.append({row.id:{"first":row.first_name,"last":row.last_name,"manager_status":manager_status}})
    return dict(pending_list=pending_list, approved_list=approved_list)

@auth.requires_login()
def dashboard():
    """
    This page allows a user to send an email to
    a potential customer, requesting access to bill data
    """
    i = 0
    already_submitted = False # default is false; can only be changed if request.post_vars == True
    
    # SQLFORM version
    data_requests = SQLFORM.grid(db(db.data_requests.company==auth.user.company),
                                 fields=[db.data_requests.last_name, \
                                         db.data_requests.first_name, \
                                         db.data_requests.email, \
                                         db.data_requests.status, \
                                         db.data_requests.bill_1,
                                         ],
                                 headers={'data_requests.bill_1':'Most Recent Bill'},
                                 sortable=False,
                                 create=False,
                                 editable=False,
                                 deletable=True,
                                 details=False,
                                 maxtextlength=30,
                                 csv=False,
                                 upload=URL('download'),
                                 )

    pending_list = db().select(db.data_requests.email)
    # if a new request has been made, update the database
    if request.post_vars:
        
        while not already_submitted and i <= len(data_requests):
            already_submitted = [True for row in pending_list if row.email == request.post_vars.email]
            i += 1
        
        from entergy_scraper import download
        from mailer import send_request
        
        if not already_submitted:
            # generate token
            import random, base64, sha
            token = base64.b64encode(sha.sha(str(random.random())).hexdigest())[:8]
            db.tokens.insert(token = token,
                             first_name = request.post_vars.first_name,
                             last_name = request.post_vars.last_name,
                             email = request.post_vars.email,
                             company = auth.user.company,
                             )
            
            # send request to user
            # db.scheduler_task.insert(status = 'QUEUED',
                                     # application_name = 'nolabills/default',
                                     # task_name = 'request',
                                     # group_name = 'email',
                                     # function_name = 'send_request',
                                     # args = '["{0}", "{1}", "{2}", "{3}"]'.format( \
                                         # request.post_vars.first, request.post_vars.last, request.post_vars.email, token),
                                     # vars = '{}',
                                     # enabled = True,
                                     # start_time = request.now,
                                     # timeout = 500, # should take less than 500 secs
                                     # )
            # update data_requests to show that the task is AWAITING_CUSTOMER
            db.data_requests.insert(first_name = request.post_vars.first_name,
                                    last_name = request.post_vars.last_name,
                                    email = request.post_vars.email,
                                    company = auth.user.company,
                                    status = 'AWAITING_CUSTOMER',
                                    )


    return dict(already_submitted=already_submitted, data_requests=data_requests, pending_list=pending_list)

def approve_request():
    """
    Allows a customer to approve a data request
    """
    submitted = 'submitted'
    valid_token = 'valid_token'
    invalid_token = 'invalid_token'
    
    
    # if the user has submitted their data and it's not already in the database, let them know we'll get their data
    if request.post_vars:
        status = submitted
        
        token_submitted = True if db(db.data_requests.email == request.post_vars.email).select(db.tokens.submitted).first() else False
        
        if token_submitted == False:
        # download data
            db.data_requests.insert(first_name = request.post_vars.first_name,
                                    last_name = request.post_vars.last_name,
                                    email = request.post_vars.email,
                                    company = auth.user.company,
                                    entergy_username = request.post_vars.entergy_username,
                                    entergy_password = request.post_vars.entergy_password,
                                    )
    
    # if no submission of info, try to give them the option to do so
    else:
        try:
            status = valid_token
            token = request.get_vars.token
            token_row = db(db.tokens.token == token).select().first()
            first_name = token_row.first_name
            last_name = token_row.last_name
            email = token_row.email
        except:
            status = invalid_token

    if status == submitted or status == invalid_token:
        return dict(status=status, submitted=submitted, invalid_token=invalid_token, token_submitted=token_submitted)
    else:
        return dict(status=status, first_name=first_name, last_name=last_name, email=email, submitted=False, invalid_token=False, token_submitted=False)
    
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)

def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()

# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

## app configuration made easy. Look inside private/appconfig.ini
from gluon.contrib.appconfig import AppConfig
## import current for current.db = db line
from gluon import current

## once in production, remove reload=True to gain full speed
myconf = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL(myconf.take('db.uri'), pool_size=myconf.take('db.pool_size', cast=int), check_reserved=['all'], migrate_enabled=True)
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore+ndb')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))


## define current.db for module usage
current.db = db

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## choose a style for forms
response.formstyle = myconf.take('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.take('forms.separator')


## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Service, PluginManager

auth = Auth(db)
service = Service()
plugins = PluginManager()
  
auth.settings.extra_fields['auth_user']= [
  Field('company', type='string')]

## validators
# custom_auth_table = db[auth.settings.table_user_name] # get the custom auth table
# custom_auth_table.first_name.requires = IS_NOT_EMPTY(error_message=auth.messages.is_empty)
# custom_auth_table.last_name.requires = IS_NOT_EMPTY(error_message=auth.messages.is_empty)
# custom_auth_table.company.requires = IS_NOT_EMPTY(error_message=auth.messages.is_empty)
# custom_auth_table.password.requires = CRYPT()
# custom_auth_table.email.requires = [
    # IS_EMAIL(error_message=auth.messages.invalid_email),
    # IS_NOT_IN_DB(db, custom_auth_table.email),
    # ]
                    
## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False, migrate=False, fake_migrate=True)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'smtp.gmail.com'
mail.settings.sender = 'murtin.jones@gmail.com'
mail.settings.login = 'murtin.jones@gmail.com:bnxcduttlnwqaldc'

## configure auth policy
auth.settings.registration_requires_verification = True
auth.settings.registration_requires_approval = True
auth.settings.reset_password_requires_verification = True

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)


## create list of companies for which this is available
db.define_table('company_info',
                Field('name'),
                Field('street_address'),
                )

## create a table for the queue of not yet fulfilled data requests
db.define_table('data_requests',
                Field('first_name'),
                Field('last_name'),
                Field('entergy_username'),
                Field('entergy_password'),
                Field('email'),
                Field('company'),
                Field('status', default='QUEUED'),
                Field('runs', 'integer', default=0),
                Field('bill_1', 'upload', uploadfield='bill_1_file'),
                Field('bill_1_file', 'blob'),
                )


## create a table for generating tokens
db.define_table('tokens',
                Field('first_name'),
                Field('last_name'),
                Field('email'),
                Field('company'),
                Field('token'),
                Field('submitted', default=False),
                )
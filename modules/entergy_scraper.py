# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import time
from pyvirtualdisplay import Display
import os


class download:
    def __init__(self, username, password, last_name, first_name, email):
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()
        import pyautogui # MUST be imported only after starting the display
        """ Launch the browser and go to the website """        
        self.path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'static', 'PDFs', str(username + "_" + last_name)))
        self.profile = webdriver.FirefoxProfile()
        self.profile.set_preference("browser.download.folderList", 2)
        self.profile.set_preference("browser.download.dir", self.path)
        self.browser = webdriver.Firefox(firefox_profile=self.profile)
        self.browser.get("http://www.entergy.com")
        self.username, self.password, self.last_name, self.first_name, self.email \
            = username, password, last_name, first_name, email
        
    def tryLogin(self, attempts = 0, username_elem = "login_actionForm_userId", password_elem = "login_actionForm_password", login_button_xpath = "//*[@name='LoginMAO']"):
        """ Wait for login page to load, then log into website """
        self.attempts = attempts
        time.sleep(5)
        self.username_elem = self.browser.find_element_by_id(username_elem)
        self.password_elem = self.browser.find_element_by_id(password_elem)
        self.username_elem.send_keys(self.username)
        self.password_elem.send_keys(self.password)
        self.login_button = self.browser.find_element_by_xpath(login_button_xpath)
        self.login_button.click()
        
        # if still loading after 5 seconds, refresh
        time.sleep(5)
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, "lxml")
        should_refresh = True if "Redirecting..." in str(self.soup) else False
        if should_refresh:
            self.browser.refresh()
        
        # check if login succeeded or not. If not, run two more attempts
        time.sleep(5)
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, "lxml")
        self.logged_in = False if "Login failed" in self.soup.get_text() else True
        if self.logged_in == False and self.attempts < 3:
            print self.attempts
            self.attempts += 1
            self.tryLogin(self.attempts, "userid", "password", "//*[@id='login']")
        elif self.logged_in == False and attempts == 3:
            print "Tried to login three times and failed."
        else:
            print "Logged in."
    
    def getBillPage(self):
        """ Wait for page to load, then get link to bill and history page and go to the link """
        time.sleep(5)
        self.soup = BeautifulSoup(self.browser.page_source, "lxml")
        self.parent = self.soup.find(id='billing').parent
        self.parent_link = self.parent['href']
        self.browser.get("https://myaccount.entergy.com"+str(self.parent_link))
        
    def downloadBills(self, i = 1):
        """ Navigate to each bill page (up to 12) and download each """
        time.sleep(3)
        self.i = i
        self.soup = BeautifulSoup(self.browser.page_source, "lxml")
        self.bill_links = self.soup.find_all('a', text=" Click to View Bill")
        self.num_pages = self.soup.find('div', {"id":"ext-comp-1007"}).getText()
        for each in self.bill_links:
            # if the last twelve months have already been downloaded, break
            if self.i == 12:
                break
            # if <12 bills saved, and the loop is on the final bill of current page, 
            # and there's more than one page of bills, go to the next page
            elif each == self.bill_links[-1] and self.num_pages != "of 1":
                self.next_btn = "//button[@class=' x-btn-text x-tbar-page-next']"
                self.next_btn = self.browser.find_element_by_xpath(self.next_btn)
            else:
                self.next_btn = False
                
            # open each bill
            self.link = "https://myaccount.entergy.com" + each['href']
            self.browser.get(self.link)
            # save each bill
            time.sleep(1)
            self.browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 's')
            time.sleep(3)
            self.filename = str(self.username.lower() + "_" + self.last_name.lower() + "_" + str(self.i))
            pyautogui.typewrite(self.filename)
            time.sleep(3)
            pyautogui.press('enter')
            # time.sleep(0.5)
            # pyautogui.press('enter')
                
            if self.next_btn:
                self.next_btn.click()
                self.downloadBills(i = self.i)
            else:
                self.browser.back()
                self.i += 1
                time.sleep(0.5)
            
    def close(self):
        print "Closing..."
        self.browser.quit()
        self.display.stop()

def download_func(username, password, last_name, first_name, email):
    ''' Creates an instance of the download class '''
    instance = download(username, password, last_name, first_name, email)
    instance.tryLogin()
    instance.getBillPage()
    instance.downloadBills()
    instance.close()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *
from gluon import current
import os

class organizer:
    def __init__(self):
        self.db = current.globalenv['db']
    
    def returnListByStatus(self, status):
        return self.db(self.db.data_requests.status == status).select()
        
    def checkForDownload(self, fpath):
        return os.path.isfile(fpath)
    
    def openFile(self, fpath):
        return open(fpath, 'rb')
    
    def insertTask(self, username, password, last_name, first_name, email):
        self.db.scheduler_task.insert(status = 'QUEUED', \
                                      application_name = 'nolabills/default', \
                                      task_name = 'download', \
                                      group_name = 'scrape', \
                                      function_name = 'download',\
                                      args = '[{0}, {1}, {2}, {3}, {4}]'.format( \
                                              username, password, last_name, first_name, email), \
                                      vars = '{}', \
                                      )
        self.db.commit()

    def updateRunningRequests(self):
        self.running_data_requests = self.returnListByStatus('RUNNING')
        
        for row in self.running_data_requests:
            self.username = row.entergy_username
            self.last_name = row.last_name
            self.fname = self.username.lower() + "_" + self.last_name.lower()
            self.fpath = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'uploads', 'PDFs', self.fname, self.fname + "_1" + ".pdf"))
            
            if self.checkForDownload(self.fpath) == False and row.runs == 3:
                row.update_record(status = 'FAILED')
            elif self.checkForDownload(self.fpath) == True:
                self.stream = self.openFile(self.fpath)
                row.update_record(bill_1 = self.db.data_requests.bill_1.store(self.stream, self.fname+".pdf"), bill_1_file=self.stream.read())
                row.update_record(status = 'COMPLETED')
            else: 
                # self.insertTask(row.entergy_username, row.entergy_password, row.last_name, row.first_name, row.email)
                row.runs = row.runs + 1
                row.status = 'RUNNING'
                row.update_record()
                # commit any changes to db
            self.db.commit()
        
    def updateQueuedRequests(self):
        self.queued_data_requests = self.returnListByStatus('QUEUED')
        
        for row in self.queued_data_requests:
            # self.insertTask(row.entergy_username, row.entergy_password, row.last_name, row.first_name, row.email)
            row.update_record(status = 'RUNNING')
            row.runs = row.runs + 1
            row.update_record()
            # commit any changes to db
            self.db.commit()
            
def organizer_func():
    instance = organizer()
    instance.updateRunningRequests()
    instance.updateQueuedRequests()